.PHONY all: clean object test $(bin) exec
bin = main main2 main3 main4
prefix = nnet nmath nlist nassert nconv
gcc = gcc -Iinclude -Wall -g
clean:
	rm -rf *.o
object:
	for target in $(prefix) ; do \
		$(gcc) -c $$target.c -o $$target.o ; \
	done
test: object
	make -C test -f makefile
main: main.c nnet.o nmath.o nlist.o
	$(gcc) $^ -lm -o $@
main2: main2.c nconv.o nmath.o nlist.o
	$(gcc) $^ -lm -o $@
main3: main3.c nconv.o nmath.o nlist.o
	$(gcc) $^ -lm -o $@
main4: main4.c nconv.o nmath.o nlist.o
	$(gcc) $^ -lm -o $@
exec: $(bin)
	sh test/runValgrind.sh $^
