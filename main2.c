#include <nconv.h>
#include <nlist.h>
#include <nmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

int readTrainData(ConvNet * cNet, char * filename) {
	FILE * fp = fopen(filename, "r");
	if(!fp) return -1;
	for(unsigned int i = 0; i < cNet->dimConvNet->countInputX; i++)
		for(unsigned int j = 0; j < cNet->dimConvNet->countInputY; j++)
			if(fscanf(fp, "%lf", &(cNet->input[i][j])) == EOF) return -1;
	printf("input:\n");
	for(unsigned int i = 0; i < cNet->dimConvNet->countInputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countInputY; j++) {
			printf("%1.0lf ", cNet->input[i][j]);
		}
		printf("\n");
	}
	fclose(fp);
	return 0;
}

int readFilterData(ConvNet * cNet, char * filename) {
	FILE * fp = fopen(filename, "r");
	if(!fp) return -1;
	for(unsigned int i = 0; i < cNet->dimConvNet->countFilterX; i++)
		for(unsigned int j = 0; j < cNet->dimConvNet->countFilterY; j++)
			if(fscanf(fp, "%lf", &(cNet->filter[i][j])) == EOF) return -1;
	printf("filter:\n");
	for(unsigned int i = 0; i < cNet->dimConvNet->countFilterX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countFilterY; j++) {
			printf("%1.0lf ", cNet->filter[i][j]);
		}
		printf("\n");
	}
	fclose(fp);
	return 0;
}

void run(char * tDataName, char * filterName) {
	DimConvNet dimConvNet;
	dimConvNet.countInputX = 11;
	dimConvNet.countInputY = 11;
	dimConvNet.countOutputX = 9;
	dimConvNet.countOutputY = 9;
	dimConvNet.countFilterX = 3;
	dimConvNet.countFilterY = 3;
	dimConvNet.countPad = 0;
	dimConvNet.countStride = 1;
	ConvNet * cNet = calloc(1, sizeof(ConvNet));
	if(!cNet) exit(EXIT_FAILURE);
	if(initConvNet(cNet, &dimConvNet) == NULL) exit(EXIT_FAILURE);
	cNet->forward = convForward;
	cNet->activate = relu;
	readTrainData(cNet, tDataName);
	readFilterData(cNet, filterName);
	cNet->forward(cNet);
	printf("convRes:\n");
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++) {
			printf("%1.0lf ", cNet->output[i][j]);
		}
		printf("\n");
	}
	DimPoolNet dimPoolNet;
	dimPoolNet.countInputX = dimConvNet.countOutputX;
	dimPoolNet.countInputY = dimConvNet.countOutputY;
	dimPoolNet.countPoolX = 3;
	dimPoolNet.countPoolY = 3;
	dimPoolNet.countOutputX = 3;
	dimPoolNet.countOutputY = 3;
	dimPoolNet.countStride = 3;
	PoolNet * pNet = calloc(1, sizeof(PoolNet));
	if(!pNet) exit(EXIT_FAILURE);
	if(initPoolNet(pNet, &dimPoolNet) == NULL) exit(EXIT_FAILURE);
	pNet->pool = poolMax;
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++)
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++)
			pNet->input[i][j] = cNet->output[i][j];
	pNet->pool(pNet);
	printf("poolRes:\n");
	for(unsigned int i = 0; i < pNet->dimPoolNet->countOutputX; i++) {
		for(unsigned int j = 0; j < pNet->dimPoolNet->countOutputY; j++) {
			printf("%1.0lf ", pNet->output[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	freePoolNet(pNet);
	freeConvNet(cNet);
}

int main(void) {
	char dataName[3][20];
	char filterName[2][20];
	for(unsigned int i = 0; i < 3; i++) sprintf(dataName[i], "tData/conv%d.dat", i + 1);
	for(unsigned int i = 0; i < 2; i++) sprintf(filterName[i], "tData/convFilt%d.dat", i + 1);
	for(unsigned int i = 0; i < 3; i++)
		for(unsigned int j = 0; j < 2; j++)
			run(dataName[i], filterName[j]);
}
