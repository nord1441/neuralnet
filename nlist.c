#include <stdio.h>
#include <stdlib.h>
#include <nlist.h>
#include <time.h>

int addTail(List * list) {
	if(!list) return -1;
	Node * newNode = calloc(1, sizeof(Node));
	if(!newNode) return -1;
	if(!list->head || !list->tail) {
		list->head = newNode;
	} else {
		Node * target = list->tail;
		target->next = newNode;
		newNode->prev = target;
	}
	list->tail = newNode;
	return 0;
}

int delTail(List * list) {
	if(!list) return -1;
	if(!list->head || !list->tail) return -1;
	if (list->tail == list->head){
		free(list->head);
		list->head = NULL;
		list->tail = NULL;
	} else {
		Node * target = list->tail;
		Node * newTail = target->prev;
		newTail->next = NULL;
		list->tail = newTail;
		free(target);
	}
	return 0;
}

