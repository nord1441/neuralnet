#ifndef _NetList
#define _NetList

typedef struct _Node {
	struct _Node * prev;
	struct _Node * next;
	void * data;
} Node;

typedef struct {
	Node * head;
	Node * tail;
} List;

int addTail(List *);
int delTail(List *);
#endif
