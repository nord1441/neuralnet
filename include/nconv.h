
#ifndef _ConvNet
#define _ConvNet
#include <stdio.h>
#include <nlist.h>

typedef struct _dimConvNet {
	unsigned int countInputX, countInputY;
	unsigned int countOutputX, countOutputY;
	unsigned int countFilterX, countFilterY;
	unsigned int countPad, countStride;
} DimConvNet;

typedef struct _convNet {
	DimConvNet * dimConvNet;
	double bias;
	double ** filter;
	double ** input;
	double ** rawOutput;
	double ** output;
	double ** errorInput;
	double ** errorOutput;
	double alpha;
	double randMax;
	double ** (*forward)(struct _convNet *);
	double ** (*backward)(struct _convNet *);
	double (*activate)(double);
	double (*deactivate)(double);
} ConvNet;

typedef struct _dimPoolNet {
	unsigned int countInputX, countInputY;
	unsigned int countPoolX, countPoolY;
	unsigned int countOutputX, countOutputY;
	unsigned int countStride;
} DimPoolNet;

typedef struct _poolNet {
	DimPoolNet * dimPoolNet;
	double ** input;
	double ** output;
	double ** (*pool)(struct _poolNet *);
} PoolNet;

void freeConvNet(ConvNet *);
ConvNet * initConvNet(ConvNet *, DimConvNet *);
ConvNet * initConvWeight(ConvNet *);
int checkDimConvNet(DimConvNet *);
double ** convForward(ConvNet *);
double ** convBackward(ConvNet *);
ConvNet * copyConvNet(ConvNet *);
ConvNet * copyConvWeight(ConvNet *, ConvNet *);
ConvNet * readConvNet(FILE *);
int writeConvNet(FILE *, ConvNet *);
double ** convPredict(List *, double **, unsigned int);
double ** convLoss(List *, double **, unsigned int);
List * createConvNetwork(unsigned int, unsigned int *);
void destroyConvNetwork(List *);
List * importConvNetwork(char *);
int exportConvNetwork(List *, char *);
void freePoolNet(PoolNet *);
PoolNet * initPoolNet(PoolNet *, DimPoolNet *);
double ** poolMax(PoolNet *);
#endif

