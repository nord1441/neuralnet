#ifndef _Net
#define _Net
#include <stdio.h>
#include <nlist.h>

typedef struct _net {
	unsigned int countInput, countOutput;
	double * bias;
	double ** weight;
	double * input;
	double * rawOutput;
	double * output;
	double * errorInput;
	double * errorOutput;
	double alpha;
	double randMax;
	double * (*forward)(struct _net *);
	double * (*backward)(struct _net *, struct _net *);
	double (*activate)(double);
	double (*deactivate)(double);
	void (* freeNet)(struct _net *);
} Net;

void freeNet(Net *);
void freeSharedWeightNet(Net *);
Net * initNet(Net *, unsigned int, unsigned int);
Net * initWeight(Net *);
double * forward(Net *);
double * backward(Net *, Net *);
void updateWeights(Net *);
void cancelUpdateWeights(Net *);
void setSharedWeights(Net *, Net *);
void accumulateWeights(Net *, Net *);
void updateErrors(Net *);
void applyAccumulatedWeights(Net *, Net *);
Net * copyNet(Net *);
Net * copyWeight(Net *, Net *);
Net * readNet(FILE *);
int writeNet(FILE *, Net *);
double * predict(List *, double *, unsigned int);
double * loss(List *, List *, double *, unsigned int);
void applyAccumulated(List *, List *);
List * createNetwork(unsigned int, unsigned int *);
void destroyNetwork(List *);
List * importNetwork(char *);
int exportNetwork(List *, char *);
#endif

