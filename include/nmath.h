#ifndef _NetMath
#define _NetMath

double normRand(double, double);
double sigmoid(double);
double sigmoid_b(double);
double tanh(double);
double tanh_b(double);
double step(double);
double relu(double);
double relu_b(double);
double identity(double);
double identity_b(double);
double meanSquaredError(double *, double *, unsigned int);
double crossEntropyError(double *, double *, unsigned int);
#endif
