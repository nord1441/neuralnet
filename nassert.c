#include <nassert.h>
#include <assert.h>

void assertEqual(double value1, double value2, double error) {
        double temp = value1 - value2;
        if(temp < 0) temp *= -1;
        if(error < 0) error *= -1;
        assert(temp < error);
}

void assertLess(double value1, double value2) {
        double temp = value2 - value1;
        assert(temp > 0);
}

