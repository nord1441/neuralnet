#include <nmath.h>
#include <nnet.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

double sigmoid(double input) {
	return 1 / (1 + exp(-1 * input));
}

double sigmoid_b(double input) {
	return sigmoid(input) * (1 - sigmoid(input));
}

double tanh(double input) {
	return 2 / (1 + exp(-1 * input)) - 1;
}

double tanh_b(double input) {
	return 2 * sigmoid(input) * (1 - sigmoid(input));
}

double step(double input) {
	if(input > 0) return 1;
	else return 0;
}

double relu(double input) {
	if(input > 0) return input;
	else return 0;
}

double relu_b(double input) {
	if(input > 0) return 1;
	else return 0;
}

double identity(double input) {
	return input;
}

double identity_b(double input) {
	return 1;
}

double meanSquaredError(double * y, double * t, unsigned int count) {
	double ret = 0;
	for(unsigned int i = 0; i < count; i++) {
		double diff = y[i] - t[i];
		ret += diff * diff;
	}
	ret *= 0.5;
	return ret;
}

double crossEntropyError(double * y, double * t, unsigned int count) {
	double ret = 0;
	for(unsigned int i = 0; i < count; i++)
		ret += t[i] * log(y[i] + 1e-7);
	ret *= -1;
	return ret;
}

