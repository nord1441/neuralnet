#include <stdio.h>
#include <assert.h>

void testReadMnist(char * filename) {
//	readMnist()
	FILE * fp = fopen(filename, "r");
	assert(fp);
	fclose(fp);
}

int main(void) {
	char * filename = "train-images-idx3-ubyte";
	testReadMnist(filename);
}
