#include <stdio.h>
#include <assert.h>

int main(void) {
	FILE * fp = fopen("train-images-idx3-ubyte", "r");
	assert(fp);
	typedef union {
		int value;
		unsigned char bit[4];
	} BEndianNumber;
	BEndianNumber magicNumber;
	for(int i = 3; i >= 0 ; i--) fread(&magicNumber.bit[i], sizeof(unsigned char), 1, fp);
	printf("%d\n", magicNumber.value);
	BEndianNumber numImages;
	for(int i = 3; i >= 0 ; i--) fread(&numImages.bit[i], sizeof(unsigned char), 1, fp);
	printf("%d\n", numImages.value);
	BEndianNumber numRow;
	for(int i = 3; i >= 0 ; i--) fread(&numRow.bit[i], sizeof(unsigned char), 1, fp);
	printf("%d\n", numRow.value);
	BEndianNumber numColumn;
	for(int i = 3; i >= 0 ; i--) fread(&numColumn.bit[i], sizeof(unsigned char), 1, fp);
	printf("%d\n", numColumn.value);
	/*for(int i = 0; i < 16; i++) {
		unsigned char bitBucket;
		fread(&bitBucket, sizeof(unsigned char), 1, fp);
	}*/
	unsigned char pixels[28][28];
	for(int k = 0; k < 10; k++){
	for(int i = 0; i < 28; i++) {
		for(int j = 0; j < 28; j++) {
			fread(&pixels[i][j], sizeof(unsigned char), 1, fp);
		}
	}
	for(int i = 0; i < 28; i++) {
		for(int j = 0; j < 28; j++) {
			if(pixels[i][j]) printf("O");
			else printf("-");
		}
		printf("\n");
	}
	}
	fclose(fp);
	return 0;
}
