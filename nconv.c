
#include <nconv.h>
#include <nlist.h>
#include <nmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void freeConvNet(ConvNet * cNet) {
	if(cNet) {
		if(cNet->input) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countInputX; i++) if(cNet->input[i]) free(cNet->input[i]);
			free(cNet->input);
		}
		if(cNet->output) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) if(cNet->output[i]) free(cNet->output[i]);
			free(cNet->output);
		}
		if(cNet->filter) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countFilterX; i++) if(cNet->filter[i]) free(cNet->filter[i]);
			free(cNet->filter);
		}
		if(cNet->rawOutput) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) if(cNet->rawOutput[i]) free(cNet->rawOutput[i]);
			free(cNet->rawOutput);
		}
		if(cNet->errorInput) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countInputX; i++) if(cNet->errorInput[i]) free(cNet->errorInput[i]);
			free(cNet->errorInput);
		}
		if(cNet->errorOutput) {
			for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) if(cNet->errorOutput[i]) free(cNet->errorOutput[i]);
			free(cNet->errorOutput);
		}
		free(cNet);
	}
}

ConvNet * initConvNet(ConvNet * cNet, DimConvNet * dimConvNet) {
	if(!cNet) return NULL;
	if(!dimConvNet) return NULL;
	if(checkDimConvNet(dimConvNet) == -1) {
		printf("dimConvNet corrupted\n");
		return NULL;
	}
	cNet->dimConvNet = dimConvNet;
	cNet->input = calloc(dimConvNet->countInputX, sizeof(double *));
	cNet->output = calloc(dimConvNet->countOutputX, sizeof(double *));
	cNet->filter = calloc(dimConvNet->countFilterX, sizeof(double *));
	cNet->rawOutput = calloc(dimConvNet->countOutputX, sizeof(double *));
	cNet->errorInput = calloc(dimConvNet->countInputX, sizeof(double *));
	cNet->errorOutput = calloc(dimConvNet->countOutputX, sizeof(double *));
	if(!cNet->input || !cNet->output || !cNet->filter || !cNet->rawOutput || !cNet->errorInput || !cNet->errorOutput) {
		freeConvNet(cNet);
		return NULL;
	}
	for(unsigned int i = 0; i < dimConvNet->countInputX; i++) {
		cNet->input[i] = calloc(dimConvNet->countInputY, sizeof(double));
		if(!cNet->input[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimConvNet->countOutputX; i++) {
		cNet->output[i] = calloc(dimConvNet->countOutputY, sizeof(double));
		if(!cNet->output[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimConvNet->countFilterX; i++) {
		cNet->filter[i] = calloc(dimConvNet->countFilterY, sizeof(double));
		if(!cNet->filter[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimConvNet->countOutputX; i++) {
		cNet->rawOutput[i] = calloc(dimConvNet->countOutputY, sizeof(double));
		if(!cNet->rawOutput[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimConvNet->countInputX; i++) {
		cNet->errorInput[i] = calloc(dimConvNet->countInputY, sizeof(double));
		if(!cNet->errorInput[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimConvNet->countOutputX; i++) {
		cNet->errorOutput[i] = calloc(dimConvNet->countOutputY, sizeof(double));
		if(!cNet->errorOutput[i]) {
			freeConvNet(cNet);
			return NULL;
		}
	}
	return cNet;
}

ConvNet * initConvWeight(ConvNet * cNet) {
	if(!cNet) return NULL;
	unsigned int countFilterX = cNet->dimConvNet->countFilterX;
	unsigned int countFilterY = cNet->dimConvNet->countFilterY;
	for(unsigned int i = 0; i < countFilterX; i++) {
		for(unsigned int j = 0; j < countFilterY; j++) {
			cNet->filter[i][j] = cNet->randMax * (drand48() * 2 - 1);
		}
	}
	cNet->bias = cNet->randMax * (drand48() * 2 - 1);
	return cNet;
}

int checkDimConvNet(DimConvNet * dimConvNet) {
	if(
		((dimConvNet->countOutputX - 1) * dimConvNet->countStride == dimConvNet->countInputX + 2 * dimConvNet->countPad - dimConvNet->countFilterX)
		&&
		((dimConvNet->countOutputY - 1) * dimConvNet->countStride == dimConvNet->countInputY + 2 * dimConvNet->countPad - dimConvNet->countFilterY)
	) return 0;
	else return -1;
}

double ** convForward(ConvNet * cNet) {
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++) {
			for(unsigned int k = 0; k < cNet->dimConvNet->countFilterX; k++) {
				for(unsigned int l = 0; l < cNet->dimConvNet->countFilterY; l++) {
					cNet->rawOutput[i][j] += cNet->input[i + k][j + l] * cNet->filter[k][l];
				}
			}
			cNet->rawOutput[i][j] += cNet->bias;
		}
	}
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++) {
			cNet->output[i][j] = cNet->activate(cNet->rawOutput[i][j]);
		}
	}
	return cNet->output;
}

double ** convBackward(ConvNet * cNet) {
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++)
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++)
			cNet->errorOutput[i][j] *= cNet->deactivate(cNet->rawOutput[i][j]);
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++) {
			for(unsigned int k = 0; k < cNet->dimConvNet->countFilterX; k++) {
				for(unsigned int l = 0; l < cNet->dimConvNet->countFilterY; l++) {
					cNet->errorInput[i + k][j + l] += cNet->errorOutput[i][j] * cNet->filter[k][l];
				}
			}
		}
	}
	for(unsigned int i = 0; i < cNet->dimConvNet->countInputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countInputY; j++) {
			for(unsigned int k = 0; k < cNet->dimConvNet->countOutputX; k++) {
				for(unsigned int l = 0; l < cNet->dimConvNet->countOutputY; l++) {
					for(unsigned int m = 0; m < cNet->dimConvNet->countFilterX; m++) {
						for(unsigned int n = 0; n < cNet->dimConvNet->countFilterY; n++) {
							cNet->filter[m][n] += cNet->alpha * cNet->errorOutput[k][l] * cNet->input[i][j];
						}
					}
				}
			}
		}
	}
	for(unsigned int i = 0; i < cNet->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet->dimConvNet->countOutputY; j++) {
			cNet->bias += cNet->alpha * cNet->errorOutput[i][j];
		}
	}
	return cNet->errorInput;
}

ConvNet * copyConvNet(ConvNet * cNet0) {
	if(!cNet0) return NULL;
	ConvNet * cNet1 = calloc(1, sizeof(ConvNet));
	if(!cNet1) return NULL;
	if(!initConvNet(cNet1, cNet0->dimConvNet)) return NULL;
        for(unsigned int i = 0; i < cNet1->dimConvNet->countInputX; i++) {
	        for(unsigned int j = 0; j < cNet1->dimConvNet->countInputY; j++) {
        	        cNet1->input[i][j] = cNet0->input[i][j];
                	cNet1->errorInput[i][j] = cNet0->errorInput[i][j];
		}
        }
	for(unsigned int i = 0; i < cNet1->dimConvNet->countFilterX; i++) {
        	for(unsigned int j = 0; j < cNet1->dimConvNet->countFilterY; j++) {
                        cNet1->filter[i][j] = cNet0->filter[i][j];
                }
        }
	cNet1->bias = cNet0->bias;
        for(unsigned int i = 0; i < cNet1->dimConvNet->countOutputX; i++) {
	        for(unsigned int j = 0; j < cNet1->dimConvNet->countOutputY; j++) {
        	        cNet1->rawOutput[i][j] = cNet0->rawOutput[i][j];
               		cNet1->output[i][j] = cNet0->output[i][j];
                	cNet1->errorOutput[i][j] = cNet0->errorOutput[i][j];
		}
        }
        cNet1->activate = cNet0->activate;
        cNet1->deactivate = cNet0->deactivate;
	cNet1->forward = cNet0->forward;
	cNet1->backward = cNet0->backward;
        cNet1->alpha = cNet0->alpha;
        return cNet1;
}

/*Net * copyWeight(Net * net0, Net * net1) {
	if(!net0) return NULL;
	if(!net1) return NULL;
	if(net0->countInput != net1->countInput) return NULL;
	if(net0->countOutput != net1->countOutput) return NULL;
	for(unsigned int i = 0; i < net0->countInput; i++) {
        	for(unsigned int j = 0; j < net0->countOutput; j++) {
                        net1->weight[i][j] = net0->weight[i][j];
                }
        }
       	for(unsigned int i = 0; i < net0->countOutput; i++) net1->bias[i] = net0->bias[i];
        return net1;
}

Net * readNet(FILE * fp) {
	if(!fp) return NULL;
	unsigned int sizeBuf = 20;
	char buf[sizeBuf];
	if(!fgets(buf, sizeBuf, fp)) return NULL;
	unsigned int countInput = atoi(buf);
	if(!fgets(buf, sizeBuf, fp)) return NULL;
	unsigned int countOutput = atoi(buf);
	Net * net = calloc(1, sizeof(Net));
	if(!net) return NULL;
	initNet(net, countInput, countOutput);
	for(unsigned int i = 0; i < countInput; i++) {
		for(unsigned int j = 0; j < countOutput; j++) {
			if(!fgets(buf, sizeBuf, fp)) return NULL;
			net->weight[i][j] = atof(buf);
		}
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		if(!fgets(buf, sizeBuf, fp)) return NULL;
		net->bias[j] = atof(buf);
	}
	return net;
}

int writeNet(FILE * fp, Net * net) {
	if(!fp) return -1;
	if(!net) return -1;
	if(fprintf(fp, "%d\n", net->countInput) == 0) return -1;
	if(fprintf(fp, "%d\n", net->countOutput) == 0) return -1;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			if(fprintf(fp, "%lf\n", net->weight[i][j]) == 0) return -1;
		}
	}
	for(unsigned int j = 0; j < net->countOutput; j++) {
		if(fprintf(fp, "%lf\n", net->bias[j]) == 0) return -1;
	}
	return 0;
}

double * predict(List * list, double * input, unsigned int count) {
	Node * current = list->head;
	if(((Net *)(current->data))->countInput != count) return NULL;
	for(unsigned int i = 0; i < count; i++) ((Net *)(current->data))->input[i] = input[i];
	while(current) {
		Net * net0 = (Net *)(current->data);
		net0->forward(net0);
		Net * net1 = NULL;
		if(current->next) net1 = (Net *)(current->next->data);
		if(net1) {
			if(net0->countOutput != net1->countInput) return NULL;
			for(unsigned int i = 0; i < net0->countOutput; i++) net1->input[i] = net0->output[i];
		}
		current = current->next;
	}
	return ((Net *)(list->tail->data))->output;
}

double * loss(List * list, double * error, unsigned int count) {
	Node * current = list->tail;
	if(((Net *)(current->data))->countOutput != count) return NULL;
	for(unsigned int j = 0; j < count; j++) ((Net *)(current->data))->errorOutput[j] = error[j];
	while(current) {
		Net * net1 = (Net *)(current->data);
		net1->backward(net1);
		Net * net0 = NULL;
		if(current->prev) net0 = (Net *)(current->prev->data);
		if(net0) {
			if(net1->countInput != net0->countOutput) return NULL;
			for(unsigned int i = 0; i < net1->countInput; i++) net0->errorOutput[i] = net1->errorInput[i];
		}
		current = current->prev;
	}
	return ((Net *)(list->head->data))->errorInput;
}

List * createNetwork(unsigned int countLayer, unsigned int * countNeuron) {
        srand48(time(NULL));
        List * list = calloc(1, sizeof(List));
        if(!list) return NULL;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                addTail(list);
        }
        Node * current = list->head;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                current->data = calloc(1, sizeof(Net));
                if(!current->data) return NULL;
                initNet(current->data, countNeuron[i], countNeuron[i + 1]);
                current = current->next;
        }
        return list;
}

void destroyNetwork(List * list) {
        Node * current = list->head;
        while(current) {
                freeNet(current->data);
                current = current->next;
        }
        while(list->head) delTail(list);
        free(list);
}

List * importNetwork(char * fileName) {
	FILE * fp = fopen(fileName, "r");
	if(!fp) return NULL;
	List * list = calloc(1, sizeof(List));
	if(!list) return NULL;
	char buf[20];
	if(!fgets(buf, 20, fp)) return NULL;
	unsigned int count = atoi(buf);
	for(unsigned int i = 0; i < count; i++) {
		Net * net = readNet(fp);
		if(net) {
			addTail(list);
			list->tail->data = net;
		} else {
			destroyNetwork(list);
			return NULL;
		}
	}
	fclose(fp);
	return list;
}

int exportNetwork(List * list, char * fileName) {
	FILE * fp = fopen(fileName, "w");
	if(!fp) return -1;
	unsigned int count = 0;
	Node * current = list->head;
	while(current) {
		count++;
		current = current->next;
	}
	fprintf(fp, "%d\n", count);
	current = list->head;
	while(current) {
		writeNet(fp, ((Net *)(current->data)));
		current = current->next;
	}
	fclose(fp);
	return 0;
}
*/
void freePoolNet(PoolNet * pNet) {
	if(pNet) {
		if(pNet->input) {
			for(unsigned int i = 0; i < pNet->dimPoolNet->countInputX; i++) if(pNet->input[i]) free(pNet->input[i]);
			free(pNet->input);
		}
		if(pNet->output) {
			for(unsigned int i = 0; i < pNet->dimPoolNet->countOutputX; i++) if(pNet->output[i]) free(pNet->output[i]);
			free(pNet->output);
		}
		free(pNet);
	}
}

PoolNet * initPoolNet(PoolNet * pNet, DimPoolNet * dimPoolNet) {
	if(!pNet) return NULL;
	if(!dimPoolNet) return NULL;
	/*if(checkDimPoolNet(dimPoolNet) == -1) {
		printf("dimPoolNet corrupted\n");
		return NULL;
	}*/
	pNet->dimPoolNet = dimPoolNet;
	pNet->input = calloc(dimPoolNet->countInputX, sizeof(double *));
	pNet->output = calloc(dimPoolNet->countOutputX, sizeof(double *));
	if(!pNet->input || !pNet->output) {
		freePoolNet(pNet);
		return NULL;
	}
	for(unsigned int i = 0; i < dimPoolNet->countInputX; i++) {
		pNet->input[i] = calloc(dimPoolNet->countInputY, sizeof(double));
		if(!pNet->input[i]) {
			freePoolNet(pNet);
			return NULL;
		}
	}
	for(unsigned int i = 0; i < dimPoolNet->countOutputX; i++) {
		pNet->output[i] = calloc(dimPoolNet->countOutputY, sizeof(double));
		if(!pNet->output[i]) {
			freePoolNet(pNet);
			return NULL;
		}
	}
	return pNet;
}

double ** poolMax(PoolNet * pNet) {
	for(int i = 0; i < pNet->dimPoolNet->countOutputX; i++)
		for(int j = 0; j < pNet->dimPoolNet->countOutputY; j++) {
			double max = -1 * __DBL_MAX__;
			for(int k = 0; k < pNet->dimPoolNet->countPoolX; k++)
				for(int l = 0; l < pNet->dimPoolNet->countPoolY; l++)
					if(max < pNet->input[i * pNet->dimPoolNet->countStride + k][j * pNet->dimPoolNet->countStride + l])
						max = pNet->input[i * pNet->dimPoolNet->countStride + k][j * pNet->dimPoolNet->countStride + l];
			pNet->output[i][j] = max;
		}
	return pNet->output;
}
