#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <nconv.h>
#include <nmath.h>
#include <nlist.h>
#include <nassert.h>

ConvNet * initTest(void) {
	ConvNet * cNet = calloc(1, sizeof(ConvNet));
	assert(cNet);
	return cNet;
}

void testFreeConvNet(void) {
	ConvNet * cNet = initTest();
	unsigned int countOutputX = 10;
	unsigned int countOutputY = 20;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	cNet->dimConvNet = dimConvNet;
	cNet->dimConvNet->countOutputX = countOutputX;
	cNet->dimConvNet->countOutputY = countOutputY;
	cNet->output = calloc(countOutputX, sizeof(double *));
	for(unsigned int i = 0; i < countOutputX; i++) cNet->output[i] = calloc(countOutputY, sizeof(double));
	freeConvNet(cNet);
	free(dimConvNet);
}

void testFreeConvNet2(void) {
	ConvNet * cNet = initTest();
	unsigned int countFilterX = 10;
	unsigned int countFilterY = 20;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	cNet->dimConvNet = dimConvNet;
	cNet->dimConvNet->countFilterX = countFilterX;
	cNet->dimConvNet->countFilterY = countFilterY;
	cNet->filter = calloc(countFilterX, sizeof(double *));
	for(unsigned int i = 0; i < countFilterX; i++) cNet->filter[i] = calloc(countFilterY, sizeof(double));
	freeConvNet(cNet);
	free(dimConvNet);
}

void testFreeConvNet3(void) {
	ConvNet * cNet = initTest();
	unsigned int countFilterX = 10;
	unsigned int countFilterY = 20;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	cNet->dimConvNet = dimConvNet;
	cNet->dimConvNet->countFilterX = countFilterX;
	cNet->dimConvNet->countFilterY = countFilterY;
	cNet->filter = calloc(countFilterX, sizeof(double *));
	cNet->filter[9] = calloc(countFilterY, sizeof(double));
	freeConvNet(cNet);
	free(dimConvNet);
}

void testInitNet(void) {
	ConvNet * cNet = initTest();
	unsigned int countInputX = 4;
	unsigned int countInputY = 4;
	unsigned int countFilterX = 3;
	unsigned int countFilterY = 3;
	unsigned int countOutputX = 2;
	unsigned int countOutputY = 2;
	unsigned int countStride = 1;
	unsigned int countPad = 0;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	dimConvNet->countInputX = countInputX;
	dimConvNet->countInputY = countInputY;
	dimConvNet->countFilterX = countFilterX;
	dimConvNet->countFilterY = countFilterY;
	dimConvNet->countOutputX = countOutputX;
	dimConvNet->countOutputY = countOutputY;
	dimConvNet->countStride = countStride;
	dimConvNet->countPad = countPad;

	assert(initConvNet(cNet, dimConvNet));
	assert(cNet->dimConvNet->countInputX == 4);
	assert(cNet->dimConvNet->countInputY == 4);
	assert(cNet->dimConvNet->countFilterX == 3);
	assert(cNet->dimConvNet->countFilterY == 3);
	assert(cNet->dimConvNet->countOutputX == 2);
	assert(cNet->dimConvNet->countOutputY == 2);
	freeConvNet(cNet);
	free(dimConvNet);
}

void testInitNet2(void) {
	ConvNet * cNet = initTest();
	unsigned int countInputX = 4;
	unsigned int countInputY = 4;
	unsigned int countFilterX = 3;
	unsigned int countFilterY = 3;
	unsigned int countOutputX = 2;
	unsigned int countOutputY = 2;
	unsigned int countStride = 1;
	unsigned int countPad = 0;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	dimConvNet->countInputX = countInputX;
	dimConvNet->countInputY = countInputY;
	dimConvNet->countFilterX = countFilterX;
	dimConvNet->countFilterY = countFilterY;
	dimConvNet->countOutputX = countOutputX;
	dimConvNet->countOutputY = countOutputY;
	dimConvNet->countStride = countStride;
	dimConvNet->countPad = countPad;

	assert(initConvNet(cNet, dimConvNet));
	cNet->input[3][3] = 1;
	assert(cNet->input[3][3] == 1);
	cNet->filter[2][2] = 2;
	assert(cNet->filter[2][2] == 2);
	cNet->output[1][1] = 3;
	assert(cNet->output[1][1] == 3);
	cNet->bias = 4;
	assert(cNet->bias == 4);

	freeConvNet(cNet);
	free(dimConvNet);
}

void testConvForward(ConvNet * cNet0) {
	ConvNet * cNet1 = copyConvNet(cNet0);
	assert(cNet1);
	assert(convForward(cNet1));
	double ** assumedOutput = calloc(cNet0->dimConvNet->countOutputX, sizeof(double *));
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) assumedOutput[i] = calloc(cNet0->dimConvNet->countOutputY, sizeof(double));
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			for(unsigned int k = 0; k < cNet0->dimConvNet->countFilterX; k++) {
				for(unsigned int l = 0; l < cNet0->dimConvNet->countFilterY; l++) {
			 		assumedOutput[i][j] += cNet0->input[i + k][j + l] * cNet0->filter[k][l];
				}
			}
			assumedOutput[i][j] += cNet0->bias;
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			assumedOutput[i][j] = cNet0->activate(assumedOutput[i][j]);
		}
	}
	for(unsigned int i = 0; i < cNet1->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet1->dimConvNet->countOutputY; j++) {
			assertEqual(cNet1->output[i][j], assumedOutput[i][j], 0.0001);
		}
	}
	if(assumedOutput) {
		for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) if(assumedOutput[i]) free(assumedOutput[i]);
		free(assumedOutput);
	}
	freeConvNet(cNet1);
}

void testConvBackward(ConvNet * cNet0) {
	ConvNet * cNet1 = copyConvNet(cNet0);
	assert(cNet1);
	ConvNet * assumedConvNet = copyConvNet(cNet0);
	assert(assumedConvNet);
	assert(convForward(cNet1));
	double ** t = calloc(cNet0->dimConvNet->countOutputX, sizeof(double *));
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) t[i] = calloc(cNet0->dimConvNet->countOutputY, sizeof(double));
	t[0][0] = 0.1;
	t[1][1] = -0.2;
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++)
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			cNet1->errorOutput[i][j] = t[i][j] - cNet1->output[i][j];
			assumedConvNet->errorOutput[i][j] = t[i][j] - cNet1->output[i][j];
		}
	assert(convBackward(cNet1));
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++)
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++)
			assert(cNet1->errorOutput[i][j] == assumedConvNet->errorOutput[i][j] * cNet1->deactivate(cNet1->rawOutput[i][j]));
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			for(unsigned int k = 0; k < cNet0->dimConvNet->countFilterX; k++) {
				for(unsigned int l = 0; l < cNet0->dimConvNet->countFilterY; l++) {
					assumedConvNet->errorInput[i + k][j + l] += cNet1->errorOutput[i][j] * cNet1->filter[k][l];
				}
			}
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countInputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countInputY; j++) {
			assertEqual(assumedConvNet->errorInput[i][j], cNet1->errorInput[i][j], 0.0001);
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countInputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countInputY; j++) {
			for(unsigned int k = 0; k < cNet0->dimConvNet->countOutputX; k++) {
				for(unsigned int l = 0; l < cNet0->dimConvNet->countOutputY; l++) {
					for(unsigned int m = 0; m < cNet0->dimConvNet->countFilterX; m++) {
						for(unsigned int n = 0; n < cNet0->dimConvNet->countFilterY; n++) {
							assumedConvNet->filter[m][n] += cNet1->alpha * cNet1->errorOutput[k][l] * cNet1->input[i][j];
						}
					}
				}
			}
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countFilterX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countFilterY; j++) {
			assertEqual(assumedConvNet->filter[i][j], cNet1->filter[i][j], 0.0001);
		}
	}
	double assumedBias = 0;
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			assumedBias = cNet1->bias + cNet1->alpha * cNet1->errorOutput[i][j];
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) {
		for(unsigned int j = 0; j < cNet0->dimConvNet->countOutputY; j++) {
			assertEqual(assumedBias, cNet1->bias, 0.0001);
		}
	}
	for(unsigned int i = 0; i < cNet0->dimConvNet->countOutputX; i++) free(t[i]);
	free(t);
	freeConvNet(cNet1);
	freeConvNet(assumedConvNet);
}
/*

void testReadNet1(void) {
	char * fileName = "ReadNetTest1.net";
	FILE * fp = fopen(fileName, "r");
	assert(fp);
	Net * net = readNet(fp);
	fclose(fp);
	assert(net);
	assert(net->countInput == 2);
	assert(net->countOutput == 3);
	double weight = 0.1;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			assertEqual(net->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	double bias = -0.1;
	for(unsigned int j = 0; j < net->countOutput; j++) {
		assertEqual(net->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	freeNet(net);
}

void testReadNet2(void) {
	char * fileName = "ReadNetTest2.net";
	FILE * fp = fopen(fileName, "r");
	assert(fp);
	Net * net1 = readNet(fp);
	Net * net2 = readNet(fp);
	fclose(fp);
	assert(net1);
	assert(net1->countInput == 2);
	assert(net1->countOutput == 3);
	double weight;
	weight = 0.1;
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	double bias;
	bias = -0.1;
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	assert(net2);
	assert(net2->countInput == 2);
	assert(net2->countOutput == 3);
	weight = 0.1;
	for(unsigned int i = 0; i < net2->countInput; i++) {
		for(unsigned int j = 0; j < net2->countOutput; j++) {
			assertEqual(net2->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	bias = -0.1;
	for(unsigned int j = 0; j < net2->countOutput; j++) {
		assertEqual(net2->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	freeNet(net1);
	freeNet(net2);
}

void testWriteNet1(void) {
	FILE * fp;
	fp = fopen("ReadNetTest1.net", "r");
	assert(fp);
	Net * net1 = readNet(fp);
	assert(net1);
	fclose(fp);
	fp = fopen("WriteNetTest1.net", "w");
	assert(fp);
	int ret = writeNet(fp, net1);
	assert(ret == 0);
	fclose(fp);
	fp = fopen("WriteNetTest1.net", "r");
	assert(fp);
	Net * net2 = readNet(fp);
	assert(net2);
	fclose(fp);
	assert(net1->countInput == net2->countInput);
	assert(net1->countOutput == net2->countOutput);
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], net2->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], net2->bias[j], 0.01);
	}
	freeNet(net1);
	freeNet(net2);
}

void testWriteNet2(void) {
	FILE * fp;

	fp = fopen("ReadNetTest2.net", "r");
	assert(fp);
	Net * net1 = readNet(fp);
	assert(net1);
	Net * net2 = readNet(fp);
	assert(net1);
	fclose(fp);

	fp = fopen("WriteNetTest2.net", "w");
	assert(fp);
	int ret;
	ret = writeNet(fp, net1);
	assert(ret == 0);
	ret = writeNet(fp, net2);
	assert(ret == 0);
	fclose(fp);

	fp = fopen("WriteNetTest2.net", "r");
	assert(fp);
	Net * net3 = readNet(fp);
	assert(net3);
	Net * net4 = readNet(fp);
	assert(net4);
	fclose(fp);

	assert(net1->countInput == net3->countInput);
	assert(net1->countOutput == net3->countOutput);
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], net3->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], net3->bias[j], 0.01);
	}
	assert(net2->countInput == net4->countInput);
	assert(net2->countOutput == net4->countOutput);
	for(unsigned int i = 0; i < net2->countInput; i++) {
		for(unsigned int j = 0; j < net4->countOutput; j++) {
			assertEqual(net2->weight[i][j], net4->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net2->countOutput; j++) {
		assertEqual(net2->bias[j], net4->bias[j], 0.01);
	}
	freeNet(net1);
	freeNet(net2);
	freeNet(net3);
	freeNet(net4);
}

double * noopForward(Net * net) {
	for(unsigned int i = 0; i < net->countOutput; i++) net->output[i] = i;
	return net->output;
}

void testPredict() {
	List * list = calloc(1, sizeof(List));
	assert(list);
	Net * net0 = calloc(1, sizeof(Net));
	Net * net1 = calloc(1, sizeof(Net));
	unsigned int countInput = 3;
	unsigned int countHidden = 3;
	unsigned int countOutput = 3;
	initNet(net0, countInput, countHidden);
	initNet(net1, countHidden, countOutput);
	double * input = calloc(countInput, sizeof(double));
	assert(input);
	for(unsigned int i = 0; i < countInput; i++) input[i] = i;
	net0->forward = noopForward;
	net1->forward = noopForward;
	addTail(list);
	list->head->data = net0;
	addTail(list);
	list->tail->data = net1;
	predict(list, input, countInput);
	for(unsigned int i = 0; i < countInput; i++) assert(net0->input[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->output[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net1->input[i] == net0->output[i]);
	for(unsigned int i = 0; i < countOutput; i++) assert(net1->output[i] == i);
	free(input);
	freeNet(net0);
	freeNet(net1);
	delTail(list);
	delTail(list);
	free(list);
}

double * noopBackward(Net * net) {
	for(unsigned int i = 0; i < net->countInput; i++) net->errorInput[i] = i;
	return net->errorInput;
}

void testLoss() {
	List * list = calloc(1, sizeof(List));
	assert(list);
	Net * net0 = calloc(1, sizeof(Net));
	Net * net1 = calloc(1, sizeof(Net));
	unsigned int countInput = 3;
	unsigned int countHidden = 3;
	unsigned int countOutput = 3;
	initNet(net0, countInput, countHidden);
	initNet(net1, countHidden, countOutput);
	double * error = calloc(countOutput, sizeof(double));
	assert(error);
	for(unsigned int i = 0; i < countOutput; i++) error[i] = i;
	net0->backward = noopBackward;
	net1->backward = noopBackward;
	addTail(list);
	list->head->data = net0;
	addTail(list);
	list->tail->data = net1;
	loss(list, error, countOutput);
	for(unsigned int i = 0; i < countOutput; i++) assert(net1->errorOutput[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net1->errorInput[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->errorOutput[i] == net1->errorInput[i]);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->errorInput[i] == i);
	free(error);
	freeNet(net0);
	freeNet(net1);
	delTail(list);
	delTail(list);
	free(list);
}

void testCreateNetwork(unsigned int countLayer, unsigned int * countNeuron) {
        List * list = createNetwork(countLayer, countNeuron);
        Node * current = list->head;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                assert(((Net *)(current->data))->countInput == countNeuron[i]);
                assert(((Net *)(current->data))->countOutput == countNeuron[i + 1]);
                current = current->next;
        }
        destroyNetwork(list);
}

void testImportNetwork1(void) {
	List * list = importNetwork("ImportNetworkTest1.net");
	assert(list);
	assert(((Net *)(list->head->data))->countInput == 2);
	assert(((Net *)(list->head->data))->countOutput == 3);
	destroyNetwork(list);
}

void testImportNetwork2(void) {
	List * list = importNetwork("ImportNetworkTest2.net");
	assert(list);
	assert(((Net *)(list->head->data))->countInput == 2);
	assert(((Net *)(list->head->data))->countOutput == 3);
	assert(((Net *)(list->tail->data))->countInput == 3);
	assert(((Net *)(list->tail->data))->countOutput == 6);
	destroyNetwork(list);
}

void testExportNetwork1(void) {
	List * list1 = importNetwork("ImportNetworkTest1.net");
	assert(list1);
	assert(exportNetwork(list1, "ExportNetworkTest1.net") == 0);
	List * list2 = importNetwork("ExportNetworkTest1.net");
	assert(list2);
	assert(((Net *)(list1->head->data))->countInput == ((Net *)(list2->head->data))->countInput);
	assert(((Net *)(list1->head->data))->countOutput == ((Net *)(list2->head->data))->countOutput);
	destroyNetwork(list1);
	destroyNetwork(list2);
}

void testExportNetwork2(void) {
	List * list1 = importNetwork("ImportNetworkTest2.net");
	assert(list1);
	assert(exportNetwork(list1, "ExportNetworkTest2.net") == 0);
	List * list2 = importNetwork("ExportNetworkTest2.net");
	assert(list2);
	assert(((Net *)(list1->head->data))->countInput == ((Net *)(list2->head->data))->countInput);
	assert(((Net *)(list1->head->data))->countOutput == ((Net *)(list2->head->data))->countOutput);
	assert(((Net *)(list1->tail->data))->countInput == ((Net *)(list2->tail->data))->countInput);
	assert(((Net *)(list1->tail->data))->countOutput == ((Net *)(list2->tail->data))->countOutput);
	destroyNetwork(list1);
	destroyNetwork(list2);
}
*/

void testCheckDimConvNet(void) {
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	dimConvNet->countInputX = 4;
	dimConvNet->countInputY = 4;
	dimConvNet->countFilterX = 3;
	dimConvNet->countFilterY = 3;
	dimConvNet->countOutputX = 2;
	dimConvNet->countOutputY = 2;
	dimConvNet->countStride = 1;
	dimConvNet->countPad = 0;
	assert(checkDimConvNet(dimConvNet) == 0);
	free(dimConvNet);
}

void testCheckDimConvNet2(void) {
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	dimConvNet->countInputX = 4;
	dimConvNet->countInputY = 4;
	dimConvNet->countFilterX = 3;
	dimConvNet->countFilterY = 3;
	dimConvNet->countOutputX = 2;
	dimConvNet->countOutputY = 5;
	dimConvNet->countStride = 1;
	dimConvNet->countPad = 0;
	assert(checkDimConvNet(dimConvNet) == -1);
	free(dimConvNet);
}

void testPoolMax(void) {
	PoolNet * pNet = calloc(1, sizeof(PoolNet));
	DimPoolNet dimPoolNet;
	dimPoolNet.countInputX = 4;
	dimPoolNet.countInputY = 4;
	dimPoolNet.countPoolX = 2;
	dimPoolNet.countPoolY = 2;
	dimPoolNet.countOutputX = 2;
	dimPoolNet.countOutputY = 2;
	dimPoolNet.countStride = 2;
	initPoolNet(pNet, &dimPoolNet);
	pNet->pool = poolMax;
	pNet->input[0][0] = 1;
	pNet->input[0][1] = 2;
	pNet->input[0][2] = 1;
	pNet->input[0][3] = 0;
	pNet->input[1][0] = 0;
	pNet->input[1][1] = 1;
	pNet->input[1][2] = 2;
	pNet->input[1][3] = 3;
	pNet->input[2][0] = 3;
	pNet->input[2][1] = 0;
	pNet->input[2][2] = 1;
	pNet->input[2][3] = 2;
	pNet->input[3][0] = 2;
	pNet->input[3][1] = 4;
	pNet->input[3][2] = 0;
	pNet->input[3][3] = 1;
	pNet->pool(pNet);
	assert(pNet->output[0][0] == 2);
	assert(pNet->output[0][1] == 3);
	assert(pNet->output[1][0] == 4);
	assert(pNet->output[1][1] == 2);
	freePoolNet(pNet);
}

int main(void) {
	testFreeConvNet();
	testFreeConvNet2();
	testFreeConvNet3();
	testInitNet();
	testInitNet2();
	ConvNet * cNet0 = initTest();
	unsigned int countInputX = 4;
	unsigned int countInputY = 4;
	unsigned int countFilterX = 3;
	unsigned int countFilterY = 3;
	unsigned int countOutputX = 2;
	unsigned int countOutputY = 2;
	unsigned int countStride = 1;
	unsigned int countPad = 0;
	DimConvNet * dimConvNet = calloc(1, sizeof(DimConvNet));
	dimConvNet->countInputX = countInputX;
	dimConvNet->countInputY = countInputY;
	dimConvNet->countFilterX = countFilterX;
	dimConvNet->countFilterY = countFilterY;
	dimConvNet->countOutputX = countOutputX;
	dimConvNet->countOutputY = countOutputY;
	dimConvNet->countStride = countStride;
	dimConvNet->countPad = countPad;
	assert(initConvNet(cNet0, dimConvNet));
	cNet0->filter[0][0] = 2;
	cNet0->filter[0][1] = 0;
	cNet0->filter[0][2] = 1;
	cNet0->filter[1][0] = 0;
	cNet0->filter[1][1] = 1;
	cNet0->filter[1][2] = 2;
	cNet0->filter[2][0] = 1;
	cNet0->filter[2][1] = 0;
	cNet0->filter[2][2] = 2;
	cNet0->bias = 3;
	cNet0->activate = sigmoid;
	cNet0->deactivate = sigmoid_b;
	cNet0->input[0][0] = 1;
	cNet0->input[0][1] = 2;
	cNet0->input[0][2] = 3;
	cNet0->input[0][3] = 0;
	cNet0->input[1][0] = 0;
	cNet0->input[1][1] = 1;
	cNet0->input[1][2] = 2;
	cNet0->input[1][3] = 3;
	cNet0->input[2][0] = 3;
	cNet0->input[2][1] = 0;
	cNet0->input[2][2] = 1;
	cNet0->input[2][3] = 2;
	cNet0->input[3][0] = 2;
	cNet0->input[3][1] = 3;
	cNet0->input[3][2] = 0;
	cNet0->input[3][3] = 1;
	cNet0->alpha = 1;
	testConvForward(cNet0);
	testConvBackward(cNet0);
	freeConvNet(cNet0);
	free(dimConvNet);
	/*Net * net1 = initTest();
	assert(initNet(net1, 2, 2));
	net1->weight[0][0] = 0.1;
	net1->weight[1][0] = 0.2;
	net1->weight[0][1] = 0.3;
	net1->weight[1][1] = 0.4;
	net1->bias[0] = 0.5;
	net1->bias[1] = 0.6;
	net1->activate = sigmoid;
	net1->deactivate = sigmoid_b;
	net1->input[0] = 0.7;
	net1->input[1] = 0.8;
	net1->alpha = 0.01;
	testForward(net1);
	testBackward(net1);
	freeNet(net1);
	Net * net2 = initTest();
	assert(initNet(net2, 3, 5));
	net2->weight[0][0] = 0.1;
	net2->weight[1][0] = 0.2;
	net2->weight[2][0] = 0.3;
	net2->weight[0][1] = 0.4;
	net2->weight[1][1] = 0.5;
	net2->weight[2][1] = 0.6;
	net2->weight[0][2] = 0.7;
	net2->weight[1][2] = 0.8;
	net2->weight[2][2] = 0.9;
	net2->weight[0][3] = 1.0;
	net2->weight[1][3] = 1.1;
	net2->weight[2][3] = 1.2;
	net2->weight[0][4] = 1.3;
	net2->weight[1][4] = 1.4;
	net2->weight[2][4] = 1.5;
	net2->bias[0] = 1.6;
	net2->bias[1] = 1.7;
	net2->bias[2] = 1.8;
	net2->bias[3] = 1.9;
	net2->bias[4] = 2.0;
	net2->activate = sigmoid;
	net2->deactivate = sigmoid_b;
	net2->input[0] = 2.1;
	net2->input[1] = 2.2;
	net2->input[2] = 2.3;
	net2->alpha = 0.01;
	testForward(net2);
	testBackward(net2);
	testReadNet1();
	testReadNet2();
	testWriteNet1();
	testWriteNet2();
	freeNet(net2);
	testPredict();
	testLoss();
	unsigned int countLayer = 3;
	unsigned int countNeuron[] = {3, 5, 7};
	testCreateNetwork(countLayer, countNeuron);
	testImportNetwork1();
	testImportNetwork2();
	testExportNetwork1();
	testExportNetwork2();
	*/
	testCheckDimConvNet();
	testCheckDimConvNet2();
	testPoolMax();
}

