#include <nassert.h>

void testAssertEqual(void) {
        assertEqual(0.005, 0.006, 0.01);
}

void testAssertLess(void) {
        assertLess(0.005, 0.006);
}

int main(void) {
	testAssertEqual();
	testAssertLess();
}
