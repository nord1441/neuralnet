#include <nmath.h>
#include <math.h>
#include <nassert.h>

void testSigmoid(void) {
	double input = 0;
	assertEqual(sigmoid(input), 1/(1 + exp(input)), 0.01);
}

void testTanh(void) {
	double input = 2;
	assertEqual(tanh(input), 2/(1 + exp(-1 * input)) - 1, 0.01);
}

void testMeanSquaredError(void) {
	double y[] = {0.3, 0.5};
	double t[] = {0.0, 1.0};
	double mse[2];
	mse[0] = meanSquaredError(y, t, 2);
	y[0] = 0.8;
	mse[1] = meanSquaredError(y, t, 2);
	assertLess(mse[0], mse[1]);
}

void testCrossEntropyError(void) {
	double y[] = {0.3, 0.5};
	double t[] = {0.0, 1.0};
	double mse[2];
	mse[0] = crossEntropyError(y, t, 2);
	y[1] = 0.01;
	mse[1] = crossEntropyError(y, t, 2);
	assertLess(mse[0], mse[1]);
}
int main(void) {
	testSigmoid();
	testTanh();
	testMeanSquaredError();
	testCrossEntropyError();
}
