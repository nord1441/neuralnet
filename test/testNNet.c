#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <nnet.h>
#include <nmath.h>
#include <nlist.h>
#include <nassert.h>

Net * initTest(void) {
	Net * net = calloc(1, sizeof(Net));
	assert(net);
	return net;
}

void testFreeNet(void) {
	Net * net = initTest();
	net->countInput = 10;
	net->bias = calloc(net->countOutput, sizeof(double));
	freeNet(net);
}

void testFreeNet2(void) {
	Net * net = initTest();
	net->countInput = 10;
	net->weight = calloc(net->countInput, sizeof(double *));
	freeNet(net);
}

void testFreeNet3(void) {
	Net * net = initTest();
	net->countInput = 10;
	net->countOutput = 20;
	net->weight = calloc(net->countInput, sizeof(double *));
	net->weight[9] = calloc(net->countOutput, sizeof(double));
	freeNet(net);
}

void testInitNet(void) {
	Net * net = initTest();
	assert(initNet(net, 10, 20));
	assert(net->countInput == 10);
	assert(net->countOutput == 20);
	freeNet(net);
}

void testInitNet2(void) {
	Net * net = initTest();
	assert(initNet(net, 10, 20));
	net->bias[19] = 2;
	assert(net->bias[19] == 2);
	freeNet(net);
}

void testInitNet3(void) {
	Net * net = initTest();
	assert(initNet(net, 10, 20));
	net->weight[9][19] = 2;
	assert(net->weight[9][19] == 2);
	freeNet(net);
}

void testForward(Net * net0) {
	unsigned int countInput = net0->countInput;
	unsigned int countOutput = net0->countOutput;
	Net * net1 = copyNet(net0);
	assert(net1);
	assert(forward(net1));
	double * assumedRawOutput = calloc(countOutput, sizeof(double));
	for(unsigned int j = 0; j < countOutput; j++) {
		for(unsigned int i = 0; i < countInput; i++) {
			 assumedRawOutput[j] += net0->input[i] * net0->weight[i][j];
		}
		assumedRawOutput[j] += net0->bias[j];
	}
	double * assumedOutput = calloc(countOutput, sizeof(double));
	for(unsigned int j = 0; j < countOutput; j++) {
		double temp;
		temp = net0->activate(assumedRawOutput[j]);
		assumedOutput[j] = temp;
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		assertEqual(net1->output[j], assumedOutput[j], 0.0001);
	}
	free(assumedOutput);
	free(assumedRawOutput);
	freeNet(net1);
}

void testBackward(Net * net0) {
	unsigned int countInput = net0->countInput;
	unsigned int countOutput = net0->countOutput;
	Net * net1 = copyNet(net0);
	assert(net1);
	assert(forward(net1));
	double * t = calloc(countOutput, sizeof(double));
	assert(t);
	t[0] = 1;
	for(unsigned int j = 0; j < countOutput; j++) net1->errorOutput[j] = meanSquaredError(net1->output, t, 1);
	double * error;
	assert((error = backward(net1, NULL)));
	double * assumedErrorInput = calloc(countInput, sizeof(double));
	assert(assumedErrorInput);
	double ** assumedWeight = calloc(countInput, sizeof(double *));
	assert(assumedWeight);
	for(unsigned int i = 0; i < countInput; i++) {
		assumedWeight[i] = calloc(countOutput, sizeof(double));
		assert(assumedWeight[i]);
	}
	double * assumedErrorOutput = calloc(countOutput, sizeof(double));
	assert(assumedErrorOutput);
	double * assumedBias = calloc(countOutput, sizeof(double));
	assert(assumedBias);
	for(unsigned int j = 0; j < countOutput; j++) {
		assumedErrorOutput[j] = net1->deactivate(net1->rawOutput[j]) * meanSquaredError(net1->output, t, 1);
		assumedBias[j] = net0->bias[j] + net1->alpha * net1->errorOutput[j];
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		for(unsigned int i = 0; i < countInput; i++) {
			assumedErrorInput[i] += net1->weight[i][j] * net1->errorOutput[j];
			assumedWeight[i][j] = net0->weight[i][j] + net1->alpha * net1->errorOutput[j] * net1->input[i];
		}
	}
	for(unsigned int i = 0; i < countInput; i++) {
		assertEqual(net1->errorInput[i], assumedErrorInput[i], 0.0001);
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		for(unsigned int i = 0; i < countInput; i++) {
			assertEqual(net1->weight[i][j], assumedWeight[i][j], 0.0001);
		}
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		assertEqual(net1->errorOutput[j], assumedErrorOutput[j], 0.0001);
		assertEqual(net1->bias[j], assumedBias[j], 0.0001);
	}
	free(t);
	free(assumedErrorInput);
	for(unsigned int i = 0; i < countInput; i++) {
		free(assumedWeight[i]);
	}
	free(assumedWeight);
	free(assumedBias);
	free(assumedErrorOutput);
	freeNet(net1);
}

void testReadNet1(void) {
	char * fileName = "ReadNetTest1.net";
	FILE * fp = fopen(fileName, "r");
	assert(fp);
	Net * net = readNet(fp);
	fclose(fp);
	assert(net);
	assert(net->countInput == 2);
	assert(net->countOutput == 3);
	double weight = 0.1;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			assertEqual(net->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	double bias = -0.1;
	for(unsigned int j = 0; j < net->countOutput; j++) {
		assertEqual(net->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	freeNet(net);
}

void testReadNet2(void) {
	char * fileName = "ReadNetTest2.net";
	FILE * fp = fopen(fileName, "r");
	assert(fp);
	Net * net1 = readNet(fp);
	Net * net2 = readNet(fp);
	fclose(fp);
	assert(net1);
	assert(net1->countInput == 2);
	assert(net1->countOutput == 3);
	double weight;
	weight = 0.1;
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	double bias;
	bias = -0.1;
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	assert(net2);
	assert(net2->countInput == 2);
	assert(net2->countOutput == 3);
	weight = 0.1;
	for(unsigned int i = 0; i < net2->countInput; i++) {
		for(unsigned int j = 0; j < net2->countOutput; j++) {
			assertEqual(net2->weight[i][j], weight, 0.01);
			weight += 0.1;
		}
	}
	bias = -0.1;
	for(unsigned int j = 0; j < net2->countOutput; j++) {
		assertEqual(net2->bias[j], bias, 0.01);
		bias -= 0.1;
	}
	freeNet(net1);
	freeNet(net2);
}

void testWriteNet1(void) {
	FILE * fp;
	fp = fopen("ReadNetTest1.net", "r");
	assert(fp);
	Net * net1 = readNet(fp);
	assert(net1);
	fclose(fp);
	fp = fopen("WriteNetTest1.net", "w");
	assert(fp);
	int ret = writeNet(fp, net1);
	assert(ret == 0);
	fclose(fp);
	fp = fopen("WriteNetTest1.net", "r");
	assert(fp);
	Net * net2 = readNet(fp);
	assert(net2);
	fclose(fp);
	assert(net1->countInput == net2->countInput);
	assert(net1->countOutput == net2->countOutput);
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], net2->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], net2->bias[j], 0.01);
	}
	freeNet(net1);
	freeNet(net2);
}

void testWriteNet2(void) {
	FILE * fp;

	fp = fopen("ReadNetTest2.net", "r");
	assert(fp);
	Net * net1 = readNet(fp);
	assert(net1);
	Net * net2 = readNet(fp);
	assert(net1);
	fclose(fp);

	fp = fopen("WriteNetTest2.net", "w");
	assert(fp);
	int ret;
	ret = writeNet(fp, net1);
	assert(ret == 0);
	ret = writeNet(fp, net2);
	assert(ret == 0);
	fclose(fp);

	fp = fopen("WriteNetTest2.net", "r");
	assert(fp);
	Net * net3 = readNet(fp);
	assert(net3);
	Net * net4 = readNet(fp);
	assert(net4);
	fclose(fp);

	assert(net1->countInput == net3->countInput);
	assert(net1->countOutput == net3->countOutput);
	for(unsigned int i = 0; i < net1->countInput; i++) {
		for(unsigned int j = 0; j < net1->countOutput; j++) {
			assertEqual(net1->weight[i][j], net3->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net1->countOutput; j++) {
		assertEqual(net1->bias[j], net3->bias[j], 0.01);
	}
	assert(net2->countInput == net4->countInput);
	assert(net2->countOutput == net4->countOutput);
	for(unsigned int i = 0; i < net2->countInput; i++) {
		for(unsigned int j = 0; j < net4->countOutput; j++) {
			assertEqual(net2->weight[i][j], net4->weight[i][j], 0.01);
		}
	}
	for(unsigned int j = 0; j < net2->countOutput; j++) {
		assertEqual(net2->bias[j], net4->bias[j], 0.01);
	}
	freeNet(net1);
	freeNet(net2);
	freeNet(net3);
	freeNet(net4);
}

double * noopForward(Net * net) {
	for(unsigned int i = 0; i < net->countOutput; i++) net->output[i] = i;
	return net->output;
}

void testPredict() {
	List * list = calloc(1, sizeof(List));
	assert(list);
	Net * net0 = calloc(1, sizeof(Net));
	Net * net1 = calloc(1, sizeof(Net));
	unsigned int countInput = 3;
	unsigned int countHidden = 3;
	unsigned int countOutput = 3;
	initNet(net0, countInput, countHidden);
	initNet(net1, countHidden, countOutput);
	double * input = calloc(countInput, sizeof(double));
	assert(input);
	for(unsigned int i = 0; i < countInput; i++) input[i] = i;
	net0->forward = noopForward;
	net1->forward = noopForward;
	addTail(list);
	list->head->data = net0;
	addTail(list);
	list->tail->data = net1;
	predict(list, input, countInput);
	for(unsigned int i = 0; i < countInput; i++) assert(net0->input[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->output[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net1->input[i] == net0->output[i]);
	for(unsigned int i = 0; i < countOutput; i++) assert(net1->output[i] == i);
	free(input);
	freeNet(net0);
	freeNet(net1);
	delTail(list);
	delTail(list);
	free(list);
}

double * noopBackward(Net * net0, Net * net1) {
	for(unsigned int i = 0; i < net0->countInput; i++) net0->errorInput[i] = i;
	return net0->errorInput;
}

void testLoss() {
	List * list = calloc(1, sizeof(List));
	assert(list);
	Net * net0 = calloc(1, sizeof(Net));
	Net * net1 = calloc(1, sizeof(Net));
	unsigned int countInput = 3;
	unsigned int countHidden = 3;
	unsigned int countOutput = 3;
	initNet(net0, countInput, countHidden);
	initNet(net1, countHidden, countOutput);
	double * error = calloc(countOutput, sizeof(double));
	assert(error);
	for(unsigned int i = 0; i < countOutput; i++) error[i] = i;
	net0->backward = noopBackward;
	net1->backward = noopBackward;
	addTail(list);
	list->head->data = net0;
	addTail(list);
	list->tail->data = net1;
	loss(list, NULL, error, countOutput);
	for(unsigned int i = 0; i < countOutput; i++) assert(net1->errorOutput[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net1->errorInput[i] == i);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->errorOutput[i] == net1->errorInput[i]);
	for(unsigned int i = 0; i < countHidden; i++) assert(net0->errorInput[i] == i);
	free(error);
	freeNet(net0);
	freeNet(net1);
	delTail(list);
	delTail(list);
	free(list);
}

void testCreateNetwork(unsigned int countLayer, unsigned int * countNeuron) {
        List * list = createNetwork(countLayer, countNeuron);
        Node * current = list->head;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                assert(((Net *)(current->data))->countInput == countNeuron[i]);
                assert(((Net *)(current->data))->countOutput == countNeuron[i + 1]);
                current = current->next;
        }
        destroyNetwork(list);
}

void testImportNetwork1(void) {
	List * list = importNetwork("ImportNetworkTest1.net");
	assert(list);
	assert(((Net *)(list->head->data))->countInput == 2);
	assert(((Net *)(list->head->data))->countOutput == 3);
	destroyNetwork(list);
}

void testImportNetwork2(void) {
	List * list = importNetwork("ImportNetworkTest2.net");
	assert(list);
	assert(((Net *)(list->head->data))->countInput == 2);
	assert(((Net *)(list->head->data))->countOutput == 3);
	assert(((Net *)(list->tail->data))->countInput == 3);
	assert(((Net *)(list->tail->data))->countOutput == 6);
	destroyNetwork(list);
}

void testExportNetwork1(void) {
	List * list1 = importNetwork("ImportNetworkTest1.net");
	assert(list1);
	assert(exportNetwork(list1, "ExportNetworkTest1.net") == 0);
	List * list2 = importNetwork("ExportNetworkTest1.net");
	assert(list2);
	assert(((Net *)(list1->head->data))->countInput == ((Net *)(list2->head->data))->countInput);
	assert(((Net *)(list1->head->data))->countOutput == ((Net *)(list2->head->data))->countOutput);
	destroyNetwork(list1);
	destroyNetwork(list2);
}

void testExportNetwork2(void) {
	List * list1 = importNetwork("ImportNetworkTest2.net");
	assert(list1);
	assert(exportNetwork(list1, "ExportNetworkTest2.net") == 0);
	List * list2 = importNetwork("ExportNetworkTest2.net");
	assert(list2);
	assert(((Net *)(list1->head->data))->countInput == ((Net *)(list2->head->data))->countInput);
	assert(((Net *)(list1->head->data))->countOutput == ((Net *)(list2->head->data))->countOutput);
	assert(((Net *)(list1->tail->data))->countInput == ((Net *)(list2->tail->data))->countInput);
	assert(((Net *)(list1->tail->data))->countOutput == ((Net *)(list2->tail->data))->countOutput);
	destroyNetwork(list1);
	destroyNetwork(list2);
}

void testCancelUpdateWeights(void) {
	Net * net0 = initTest();
	initNet(net0, 3, 3);
	initWeight(net0);
	Net * net1 = copyNet(net0);
	for(unsigned int i=0;i<net0->countInput;i++)
		for(unsigned int j=0;j<net0->countOutput;j++)
			assert(net0->weight[i][j] == net1->weight[i][j]);
	for(unsigned int j=0;j<net0->countOutput;j++)
		assert(net0->bias[j] == net1->bias[j]);
	double errorOutput[] = {0.2, 0.4, 0.6};
	for(unsigned int i=0;i<net0->countInput;i++)
		net0->errorOutput[i] = errorOutput[i];
	net0->activate = sigmoid;
	net0->deactivate = sigmoid_b;
	net0->alpha = 0.01;
	backward(net0, NULL);
	cancelUpdateWeights(net0);
	for(unsigned int i=0;i<net0->countInput;i++)
		for(unsigned int j=0;j<net0->countOutput;j++)
			assert(net0->weight[i][j] == net1->weight[i][j]);
	for(unsigned int j=0;j<net0->countOutput;j++)
		assert(net0->bias[j] == net1->bias[j]);
	freeNet(net0);
	freeNet(net1);
}

void testSetSharedWeights(void) {
	Net * net0 = initTest();
	initNet(net0, 3, 3);
	initWeight(net0);
	Net * net1 = initTest();
	initNet(net1, 3, 3);
	initWeight(net1);
	setSharedWeights(net0, net1);
	for(unsigned int i=0;i<net0->countInput;i++)
		assert(net0->weight[i] == net1->weight[i]);
	for(unsigned int i=0;i<net0->countInput;i++)
		for(unsigned int j=0;j<net0->countOutput;j++)
			net0->weight[i][j] += 0.1;
	for(unsigned int i=0;i<net0->countInput;i++)
		assert(net0->weight[i] == net1->weight[i]);
	freeNet(net0);
	freeSharedWeightNet(net1);
}

int main(void) {
	testFreeNet();
	testFreeNet2();
	testFreeNet3();
	testInitNet();
	testInitNet2();
	testInitNet3();
	Net * net0 = initTest();
	assert(initNet(net0, 2, 1));
	net0->weight[0][0] = 0.1;
	net0->weight[1][0] = 0.2;
	net0->bias[0] = 0.3;
	net0->activate = sigmoid;
	net0->deactivate = sigmoid_b;
	net0->input[0] = 0.4;
	net0->input[1] = 0.5;
	net0->alpha = 0.01;
	testForward(net0);
	testBackward(net0);
	freeNet(net0);
	Net * net1 = initTest();
	assert(initNet(net1, 2, 2));
	net1->weight[0][0] = 0.1;
	net1->weight[1][0] = 0.2;
	net1->weight[0][1] = 0.3;
	net1->weight[1][1] = 0.4;
	net1->bias[0] = 0.5;
	net1->bias[1] = 0.6;
	net1->activate = sigmoid;
	net1->deactivate = sigmoid_b;
	net1->input[0] = 0.7;
	net1->input[1] = 0.8;
	net1->alpha = 0.01;
	testForward(net1);
	testBackward(net1);
	freeNet(net1);
	Net * net2 = initTest();
	assert(initNet(net2, 3, 5));
	net2->weight[0][0] = 0.1;
	net2->weight[1][0] = 0.2;
	net2->weight[2][0] = 0.3;
	net2->weight[0][1] = 0.4;
	net2->weight[1][1] = 0.5;
	net2->weight[2][1] = 0.6;
	net2->weight[0][2] = 0.7;
	net2->weight[1][2] = 0.8;
	net2->weight[2][2] = 0.9;
	net2->weight[0][3] = 1.0;
	net2->weight[1][3] = 1.1;
	net2->weight[2][3] = 1.2;
	net2->weight[0][4] = 1.3;
	net2->weight[1][4] = 1.4;
	net2->weight[2][4] = 1.5;
	net2->bias[0] = 1.6;
	net2->bias[1] = 1.7;
	net2->bias[2] = 1.8;
	net2->bias[3] = 1.9;
	net2->bias[4] = 2.0;
	net2->activate = sigmoid;
	net2->deactivate = sigmoid_b;
	net2->input[0] = 2.1;
	net2->input[1] = 2.2;
	net2->input[2] = 2.3;
	net2->alpha = 0.01;
	testForward(net2);
	testBackward(net2);
	testReadNet1();
	testReadNet2();
	testWriteNet1();
	testWriteNet2();
	freeNet(net2);
	testPredict();
	testLoss();
	unsigned int countLayer = 3;
	unsigned int countNeuron[] = {3, 5, 7};
	testCreateNetwork(countLayer, countNeuron);
	testImportNetwork1();
	testImportNetwork2();
	testExportNetwork1();
	testExportNetwork2();
	testCancelUpdateWeights();
	testSetSharedWeights();
}

