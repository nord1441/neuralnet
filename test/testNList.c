#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <nlist.h>

void testAddTail(void) {
	List * list = calloc(1, sizeof(List));
	assert(list);
	assert(addTail(list) == 0);
	assert(list->head == list->tail);
	free(list->head);
	free(list);
}

void testAddTail2(void) {
	List * list = calloc(1, sizeof(List));
	assert(list);
	for(int i = 0; i < 2; i++) {
		assert(addTail(list) == 0);
	}
	assert(list->head->next == list->tail);
	assert(list->head == list->tail->prev);
	free(list->head);
	free(list->tail);
	free(list);
}

void testDelTail(void) {
	List * list = calloc(1, sizeof(List));
	assert(list);
	addTail(list);
	assert(delTail(list) == 0);
	assert(!list->head);
	assert(!list->tail);
	free(list);
}

void testDelTail2(void) {
	List * list = calloc(1, sizeof(List));
	assert(list);
	addTail(list);
	addTail(list);
	assert(delTail(list) == 0);
	assert(list->head == list->tail);
	assert(delTail(list) == 0);
	assert(!list->head);
	assert(!list->tail);
	free(list);
}

int main(void) {
	testAddTail();
	testAddTail2();
	testDelTail();
	testDelTail2();
}

