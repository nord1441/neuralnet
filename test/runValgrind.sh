#!/bin/sh

for x in "$@"
do
	valgrind "./$x" > "valgrind-$x.log" 2>&1
done
