#include <nnet.h>
#include <nlist.h>
#include <nmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <assert.h>

void freeNet(Net * net) {
	if(net) {
		if(net->bias) free(net->bias);
		if(net->input) free(net->input);
		if(net->rawOutput) free(net->rawOutput);
		if(net->output) free(net->output);
		if(net->errorInput) free(net->errorInput);
		if(net->errorOutput) free(net->errorOutput);
		if(net->weight) {
			for(unsigned int i = 0; i < net->countInput; i++) if(net->weight[i]) free(net->weight[i]);
			free(net->weight);
		}
		free(net);
	}
}

void freeSharedWeightNet(Net * net) {
	if(net) {
		if(net->bias) free(net->bias);
		if(net->input) free(net->input);
		if(net->rawOutput) free(net->rawOutput);
		if(net->output) free(net->output);
		if(net->errorInput) free(net->errorInput);
		if(net->errorOutput) free(net->errorOutput);
		free(net);
	}
}

Net * initNet(Net * net, unsigned int countInput, unsigned int countOutput) {
	if(!net) return NULL;
	net->freeNet = freeNet;
	net->countInput = countInput;
	net->countOutput = countOutput;
	net->bias = calloc(countOutput, sizeof(double));
	net->input = calloc(countInput, sizeof(double));
	net->rawOutput = calloc(countOutput, sizeof(double));
	net->output = calloc(countOutput, sizeof(double));
	net->errorInput = calloc(countInput, sizeof(double));
	net->errorOutput = calloc(countOutput, sizeof(double));
	net->weight = calloc(countInput, sizeof(double *));
	if(!net->bias || !net->input || !net->rawOutput || !net->output || !net->errorInput || !net->errorOutput || !net->weight) {
		net->freeNet(net);
		return NULL;
	}
	for(unsigned int i = 0; i < countInput; i++) {
		net->weight[i] = calloc(countOutput, sizeof(double));
		if(!net->weight[i]) {
			net->freeNet(net);
			return NULL;
		}
	}
	return net;
}

Net * initWeight(Net * net) {
	if(!net) return NULL;
	unsigned int countInput = net->countInput;
	unsigned int countOutput = net->countOutput;
	for(unsigned int i = 0; i < countInput; i++) {
		for(unsigned int j = 0; j < countOutput; j++) {
			net->weight[i][j] = net->randMax * (drand48() * 2 - 1);
		}
	}
	for(unsigned int i = 0; i < countOutput; i++) {
		net->bias[i] = net->randMax * (drand48() * 2 - 1);
	}
	return net;
}

double * forward(Net * net) {
	for(unsigned int j = 0; j < net->countOutput; j++) net->rawOutput[j] = 0;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
                        net->rawOutput[j] += net->input[i] * net->weight[i][j];
                }
        }
	for(unsigned int i = 0; i < net->countOutput; i++) net->rawOutput[i] += net->bias[i];
	for(unsigned int i = 0; i < net->countOutput; i++) net->output[i] = net->activate(net->rawOutput[i]);
	return net->output;
}

void updateErrors(Net * net) {
	for(unsigned int i = 0; i < net->countOutput; i++) net->errorOutput[i] *= net->deactivate(net->rawOutput[i]);
	for(unsigned int i = 0; i < net->countInput; i++) net->errorInput[i] = 0;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			net->errorInput[i] += net->errorOutput[j] * net->weight[i][j];
		}
	}
}

void updateWeights(Net * net) {
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			net->weight[i][j] += net->alpha * net->errorOutput[j] *  net->input[i];
		}
	}
	for(unsigned int i = 0; i < net->countOutput; i++) net->bias[i] += net->alpha * net->errorOutput[i];
}

void cancelUpdateWeights(Net * net) {
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			net->weight[i][j] -= net->alpha * net->errorOutput[j] *  net->input[i];
		}
	}
	for(unsigned int i = 0; i < net->countOutput; i++) net->bias[i] -= net->alpha * net->errorOutput[i];
}

void setSharedWeights(Net * net0, Net * net1) {
	for(unsigned int i = 0; i < net0->countInput; i++) {
		free(net1->weight[i]);
		//net1->weight[i] = net0->weight[i];
	}
	free(net1->weight);
	net1->weight = net0->weight;
}

void accumulateWeights(Net * net, Net * accumulated) {
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			accumulated->weight[i][j] += net->alpha * net->errorOutput[j] *  net->input[i];
		}
	}
	for(unsigned int i = 0; i < net->countOutput; i++) accumulated->bias[i] += net->alpha * net->errorOutput[i];
}

double * backward(Net * net, Net * accumulated) {
	updateErrors(net);
	if(accumulated) accumulateWeights(net, accumulated);
	updateWeights(net);
	return net->errorInput;
}

void applyAccumulatedWeights(Net * net, Net * accumulated) {
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			net->weight[i][j] += accumulated->weight[i][j];
		}
	}
	for(unsigned int i = 0; i < net->countOutput; i++) net->bias[i] += accumulated->bias[i];
}

Net * copyNet(Net * net0) {
	if(!net0) return NULL;
	unsigned int countInput = net0->countInput;
	unsigned int countOutput = net0->countOutput;
	Net * net1 = calloc(1, sizeof(Net));
	if(!net1) return NULL;
	if(!initNet(net1, countInput, countOutput)) return NULL;
        for(unsigned int i = 0; i < countInput; i++) {
                net1->input[i] = net0->input[i];
                net1->errorInput[i] = net0->errorInput[i];
        }
	for(unsigned int i = 0; i < countInput; i++) {
        	for(unsigned int j = 0; j < countOutput; j++) {
                        net1->weight[i][j] = net0->weight[i][j];
                }
        }
       	for(unsigned int i = 0; i < countOutput; i++) net1->bias[i] = net0->bias[i];
        for(unsigned int i = 0; i < countOutput; i++) {
                net1->rawOutput[i] = net0->rawOutput[i];
                net1->output[i] = net0->output[i];
                net1->errorOutput[i] = net0->errorOutput[i];
        }
        net1->activate = net0->activate;
        net1->deactivate = net0->deactivate;
	net1->forward = net0->forward;
	net1->backward = net0->backward;
        net1->alpha = net0->alpha;
        return net1;
}

Net * copyWeight(Net * net0, Net * net1) {
	if(!net0) return NULL;
	if(!net1) return NULL;
	if(net0->countInput != net1->countInput) return NULL;
	if(net0->countOutput != net1->countOutput) return NULL;
	for(unsigned int i = 0; i < net0->countInput; i++) {
        	for(unsigned int j = 0; j < net0->countOutput; j++) {
                        net1->weight[i][j] = net0->weight[i][j];
                }
        }
       	for(unsigned int i = 0; i < net0->countOutput; i++) net1->bias[i] = net0->bias[i];
        return net1;
}

Net * readNet(FILE * fp) {
	if(!fp) return NULL;
	unsigned int sizeBuf = 20;
	char buf[sizeBuf];
	if(!fgets(buf, sizeBuf, fp)) return NULL;
	unsigned int countInput = atoi(buf);
	if(!fgets(buf, sizeBuf, fp)) return NULL;
	unsigned int countOutput = atoi(buf);
	Net * net = calloc(1, sizeof(Net));
	if(!net) return NULL;
	initNet(net, countInput, countOutput);
	for(unsigned int i = 0; i < countInput; i++) {
		for(unsigned int j = 0; j < countOutput; j++) {
			if(!fgets(buf, sizeBuf, fp)) return NULL;
			net->weight[i][j] = atof(buf);
		}
	}
	for(unsigned int j = 0; j < countOutput; j++) {
		if(!fgets(buf, sizeBuf, fp)) return NULL;
		net->bias[j] = atof(buf);
	}
	return net;
}

int writeNet(FILE * fp, Net * net) {
	if(!fp) return -1;
	if(!net) return -1;
	if(fprintf(fp, "%d\n", net->countInput) == 0) return -1;
	if(fprintf(fp, "%d\n", net->countOutput) == 0) return -1;
	for(unsigned int i = 0; i < net->countInput; i++) {
		for(unsigned int j = 0; j < net->countOutput; j++) {
			if(fprintf(fp, "%lf\n", net->weight[i][j]) == 0) return -1;
		}
	}
	for(unsigned int j = 0; j < net->countOutput; j++) {
		if(fprintf(fp, "%lf\n", net->bias[j]) == 0) return -1;
	}
	return 0;
}

double * predict(List * list, double * input, unsigned int count) {
	Node * current = list->head;
	if(((Net *)(current->data))->countInput != count) return NULL;
	for(unsigned int i = 0; i < count; i++) ((Net *)(current->data))->input[i] = input[i];
	while(current) {
		Net * net0 = (Net *)(current->data);
		net0->forward(net0);
		Net * net1 = NULL;
		if(current->next) net1 = (Net *)(current->next->data);
		if(net1) {
			if(net0->countOutput != net1->countInput) return NULL;
			for(unsigned int i = 0; i < net0->countOutput; i++) net1->input[i] = net0->output[i];
		}
		current = current->next;
	}
	return ((Net *)(list->tail->data))->output;
}

double * loss(List * network, List * accumulatedWeights, double * error, unsigned int count) {
	Node * currentNetwork = network->tail;
	Node * currentAccumulated = NULL;
	if(accumulatedWeights) currentAccumulated = accumulatedWeights->tail;
	if(((Net *)(currentNetwork->data))->countOutput != count) return NULL;
	for(unsigned int j = 0; j < count; j++) ((Net *)(currentNetwork->data))->errorOutput[j] = error[j];
	while(currentNetwork) {
		Net * net = (Net *)(currentNetwork->data);
		Net * accumulated = NULL;
		if(currentAccumulated) accumulated = (Net *)(currentAccumulated->data);
		net->backward(net, accumulated);
		if(currentAccumulated) cancelUpdateWeights(net);
		Net * prevNet = NULL;
		if(currentNetwork->prev) prevNet = (Net *)(currentNetwork->prev->data);
		if(prevNet) {
			if(net->countInput != prevNet->countOutput) return NULL;
			for(unsigned int i = 0; i < net->countInput; i++) prevNet->errorOutput[i] = net->errorInput[i];
		}
		currentNetwork = currentNetwork->prev;
		if(currentAccumulated) currentAccumulated = currentAccumulated->prev;
	}
	return ((Net *)(network->head->data))->errorInput;
}

void applyAccumulated(List * network, List * accumulatedWeights) {
	Node * currentNetwork = network->tail;
	Node * currentAccumulated = accumulatedWeights->tail;
	while(currentNetwork) {
		Net * net = (Net *)(currentNetwork->data);
		Net * accumulated = (Net *)(currentAccumulated->data);
		applyAccumulatedWeights(net, accumulated);
		currentNetwork = currentNetwork->prev;
		currentAccumulated = currentAccumulated->prev;
	}
	currentAccumulated = accumulatedWeights->tail;
	while(currentAccumulated) {
		Net * accumulated = (Net *)(currentAccumulated->data);
		initWeight(accumulated);
		currentAccumulated = currentAccumulated->prev;
	}
}

List * createNetwork(unsigned int countLayer, unsigned int * countNeuron) {
        srand48(time(NULL));
        List * list = calloc(1, sizeof(List));
        if(!list) return NULL;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                addTail(list);
        }
        Node * current = list->head;
        for(unsigned int i = 0; i < countLayer - 1; i++) {
                current->data = calloc(1, sizeof(Net));
                if(!current->data) return NULL;
                initNet(current->data, countNeuron[i], countNeuron[i + 1]);
                current = current->next;
        }
        return list;
}

void destroyNetwork(List * list) {
        Node * current = list->head;
        while(current) {
		Net * currentNet = current->data;
		currentNet->freeNet(currentNet);
                current = current->next;
        }
        while(list->head) delTail(list);
        free(list);
}

List * importNetwork(char * fileName) {
	FILE * fp = fopen(fileName, "r");
	if(!fp) return NULL;
	List * list = calloc(1, sizeof(List));
	if(!list) return NULL;
	char buf[20];
	if(!fgets(buf, 20, fp)) return NULL;
	unsigned int count = atoi(buf);
	for(unsigned int i = 0; i < count; i++) {
		Net * net = readNet(fp);
		if(net) {
			addTail(list);
			list->tail->data = net;
		} else {
			destroyNetwork(list);
			return NULL;
		}
	}
	fclose(fp);
	return list;
}

int exportNetwork(List * list, char * fileName) {
	FILE * fp = fopen(fileName, "w");
	if(!fp) return -1;
	unsigned int count = 0;
	Node * current = list->head;
	while(current) {
		count++;
		current = current->next;
	}
	fprintf(fp, "%d\n", count);
	current = list->head;
	while(current) {
		writeNet(fp, ((Net *)(current->data)));
		current = current->next;
	}
	fclose(fp);
	return 0;
}
