#include <nnet.h>
#include <nlist.h>
#include <nmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

typedef struct {
	unsigned int countTrainData;
	unsigned int countInput;
	unsigned int countTeacher;
	double ** input;
	double ** teacher;
} TrainData;

void freeTrainData(TrainData * tData) {
	if(tData) {
		if(tData->input) {
			for(unsigned int i = 0; i < tData->countTrainData; i++) if(tData->input[i]) free(tData->input[i]);
			free(tData->input);
		}
		if(tData->teacher) {
			for(unsigned int i = 0; i < tData->countTrainData; i++) if(tData->teacher[i]) free(tData->teacher[i]);
			free(tData->teacher);
		}
		free(tData);
	}

}

TrainData * readTrainData(const char * filename, unsigned int countTrainData, unsigned int countInput, unsigned int countTeacher) {
	if(!filename) {
		return NULL;
	}
	TrainData * tData = calloc(1, sizeof(TrainData));
	if(!tData) return NULL;
	tData->countTrainData = countTrainData;
	tData->countInput = countInput;
	tData->countTeacher = countTeacher;
	tData->input = calloc(countTrainData, sizeof(double *));
	if(!tData->input) goto free;
	for(unsigned int i = 0; i < countTrainData; i++) {
		tData->input[i] = calloc(countInput, sizeof(double));
		if(!tData->input[i]) goto free;
	}
	tData->teacher = calloc(countTrainData, sizeof(double *));
	if(!tData->teacher) goto free;
	for(unsigned int i = 0; i < countTrainData; i++) {
		tData->teacher[i] = calloc(countTeacher, sizeof(double));
		if(!tData->teacher[i]) goto free;
	}
	FILE * fp = fopen(filename, "r");
	if(!fp) {
		perror("fopen");
		return NULL;
	}
	for(unsigned int i = 0; i < countTrainData; i++) {
		for(unsigned int j = 0; j < countInput; j++) {
			if(fscanf(fp, "%lf", &(tData->input[i][j])) == EOF) goto end;
		}
		for(unsigned int j = 0; j < countTeacher; j++) {
			if(fscanf(fp, "%lf", &(tData->teacher[i][j])) == EOF) goto end;
		}
	}
	end:
	fclose(fp);
	return tData;
	free:
	{
		freeTrainData(tData);
		return NULL;
	}
}

int main(int argc, char * argv[]) {
	unsigned int countLayer = 3;
	unsigned int countInput = 3;
	unsigned int countOutput = 1;
	unsigned int countData = 8;
	const char * filename = "tData/majority.dat";
	unsigned int countNeuron[] = {countInput, 5, countOutput};
	List * network = createNetwork(countLayer, countNeuron);
	Node * current;
	current = network->head;
	while(current) {
		((Net *)(current->data))->activate = sigmoid;
		((Net *)(current->data))->deactivate = sigmoid_b;
		((Net *)(current->data))->forward = forward;
		((Net *)(current->data))->backward = backward;
		((Net *)(current->data))->alpha = 10;
		((Net *)(current->data))->randMax = 1;
		initWeight((Net *)(current->data));
		current = current->next;
	}
	List * accumulated = createNetwork(countLayer, countNeuron);
	current = accumulated->head;
	while(current) {
		((Net *)(current->data))->activate = NULL;
		((Net *)(current->data))->deactivate = NULL;
		((Net *)(current->data))->forward = NULL;
		((Net *)(current->data))->backward = NULL;
		((Net *)(current->data))->alpha = 0;
		((Net *)(current->data))->randMax = 0;
		initWeight((Net *)(current->data));
		current = current->next;
	}
	TrainData * tData = readTrainData(filename, countData, countInput, countOutput);
	double err = 100;
	unsigned int count = 0;
	while(err > 0.001) {
		err = 0;
		for(unsigned int j = 0; j < countData; j++) {
			double * output = predict(network, tData->input[j], countInput);
			double * error = calloc(countOutput, sizeof(double));
			if(!error) return -1;
			for(unsigned int i = 0; i < countOutput; i++) error[i] = tData->teacher[j][i] - output[i];
			loss(network, accumulated, error, countOutput);
			applyAccumulated(network, accumulated);
			for(unsigned int i = 0; i < countOutput; i++) err += error[i] * error[i];
			free(error);
		}
		count++;
		fprintf(stderr, "err:%lf(%d)\n", err, count);
	}
	for(unsigned int i = 0; i < countData; i++) {
		double * output = predict(network, tData->input[i], countInput);
		for(unsigned int j = 0; j < countOutput; j++)
			printf("%dth tData[%d] = %lf, %dth output[%d] = %lf\n", i, j, tData->teacher[i][j], i, j, output[j]);
	}
	freeTrainData(tData);
	destroyNetwork(network);
	destroyNetwork(accumulated);
}
